package br.com.itau.cartao.dtos;

import br.com.itau.cartao.clients.ClienteClient;

public class CartaoDTOEntradaPost {

    private String numero;

    private long clienteId;

    public CartaoDTOEntradaPost() { }

    public String getNumero() { return numero; }

    public void setNumero(String numero) { this.numero = numero; }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }
}
