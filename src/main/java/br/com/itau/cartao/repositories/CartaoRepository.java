package br.com.itau.cartao.repositories;

import br.com.itau.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {

    Optional<Cartao> findByNumero(String numero);
    boolean existsByNumero(String numero);

}
